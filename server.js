const express = require('express');
const hbs=require('hbs');
const fs=require('fs');
const port=process.env.PORT || 3000;

let app = express();

hbs.registerPartials(__dirname+'/views/partials');
app.set('view engine','hbs');
app.use(express.static(__dirname+'/public'));
hbs.registerHelper('getCurrentYear',()=>{
    return new Date().getFullYear();
});
hbs.registerHelper('screamIt',(text)=>{

    return text.toUpperCase();
});



app.use((req,res,next)=>{

    let now= new Date().toString();
    let log=`${now}: ${req.method} ${req.url}`;
    fs.appendFile('server.log',log+'\n',err=>{
        if(err){
            console.log('Unable to append to server.log');
        }
    });
    console.log(log);

    next();
});

// app.use((req,res,next)=>{
//
//     res.render('maintain.hbs',{
//         pageTitle:'Maintenance Page',
//         mainTitle:'Maintenance Page',
//     });
// });

app.get('/',(req,res)=>{

    res.render('home.hbs',{

        pageTitle:'Home Page',
        mainTitle:'Home Page',
        welcomeMessage:'Welcome To our Site'
    });

});

app.get('/about',(req,res)=>{

    res.render('about.hbs',{
        pageTitle:'About Page',
        mainTitle:'About Page'
    });
});

app.get('/projects',(req,res)=>{

    res.render('projects.hbs',{
        pageTitle:'Projects Page',
        mainTitle:'Portfolio Page'
    });

});

app.get('/bad',(req,res)=>{

    res.send({
        errorMessage:'Unable to connect page'
    });
});

app.listen(port,()=>{
    console.log(`Server is up on port ${port}`);
});